# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap

+ Programacion declarativa 1998-2001 
++ ¿Que es la programacion declarativa?
+++ Es un paradigma de programacion que actua en relacion a los problemas que lleva la programacion clasica \nesta es la programacion en C y en pascal.
++++ Desventajas
+++++ El programador se ve obligado a dar mas informacion muy detallada sobre \nlo que se requiere realizar, esto nos dice \nque el instrumento esencial es la \nprogramacion mediante la cual se modifica el estado de la memoria y del ordenador ademas que esto es de manera detallada. \nEsto obliga tener  al programador con mas detalle sobre lo que se busca realizar.
++++ Las principales ideas son:
+++++ -Liberar al programador asignaciones  \n-Liberarse de los detalles y control permitiendo utilizar otros recursos especificos \ne los programas llevandolos a un nivel mas alto y mas cercano del como piensa un programador \n-Una ventaja muy evidente es que tenemos programas mas cortos y mas fuertes de realizar.
++ ¿Como programar sin las asignaciones? 
+++ -Se basa en el modelo de Von Neuman \n-Se buscan alternativas para decirle al ordenador que es lo que queremos usar, esto es segun los lenguajes que se ocupen. 
++ variantes de la programacion: 
+++ Programacion funcional.
++++ Se recurre al lenguaje que usan los matematicos y \nparticularmente al lenguaje que se ocupan para describir funciones.
+++ Programacion logica.
++++ En este caso se recurre a la lista de predicados en primer orden, estos son relaciones en primer orden, \nno establece orden entre argumentos de entrada y salida.
+++++ -Permite ser más declarativos porque no establece una relacion. \nLa forma de realizar computos es que un interprete dado un conjunto de relaciones y predicados.
++ Programacion perezosa o programacion comoda.
+++ Consiste en que las \ntareas rutinarias de programacion se dejan al programador, puede dejar las tareas mas rutinarias al final. \nSu propocito es reducir la complejidad de programas.
++ Evalucacion perezosa.
+++ Consiste en que los calculos no se realizan, hasta que otro posterios lo necesita sin embargo \nel compilador no calcula nada hasta el momento que lo necesite.  
++ 2001 Programacion declarativa.
+++ Para este año se decia que la programaccion declarativa se basa en una de las 2 partes \nde la programacion hace enfacis en los aspectos creativos de la programacion apartir de ideas claves y algoritmos para resolver un problema. 
++ Programacion  burocratica:
+++ Es un tipo de programacion de la cual no podemos escapar ya que normalmente es la parte en la que tenemos que hacer la gestión  \ndetallada de la memoria del ordenador para seguir una secuencia estricta de órdenes que nos  \nlleven a la solución deseada.  
++ Equlibrio entre la programacion creativa y burocratica.
+++ Creatividad y burocracia: depende del \ntipo de lenguaje y el tipo de programación.
++ Gestion de memoria. 
+++ La gestión de la memoria en código máquina es relativamente compleja hay que \ntener mucho cuidado con cada paso y cada alteración unitaria de la memoria del ordenador 	
++ 	Tipo de ventajas de la programacion declarativa. 
+++ Es que con los lenguajes declarativos que se conocen normalmente \nel tiempo de desarrollo de una aplicación es una décima parte de lo que se \ntarden programar con un lenguaje convencional imperativo y otra ventaja enorme es que el tiempo de modificación y depuración tiende a es mucho menor 
++ Funciones de orden superior.
+++ Consiste en que la entrada de un programa puede ser otro programa.
@endtmindmap
```




# Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap

+ Lenguajes de programacion 
++ Paradigmas de programacion. 
+++ El modelo de compuacion en el que los diferentes \nlenguajes datan de semantica a los programas
++ Inicialmente basandonos el el modelo de Von Neumann tambien llamado \nmodelo imperactivo. 
+++ Los programas debian estar almacenados en la computadora antes de su ejecucion. \nLa ejecicion era una serie de instrucciones que se ejecutan \nsecuencialmente, las cuales \nse pueden editar deacuerdo al estado del computo \nmendiante variables con una instruccion de asignacion.
++ POO (Programacion Orientada a Objetos.)
+++ Pequeñas secciones de codigo con instrucciones secuenciales que interactual entre si. 
++ Basado en el modelo de programacion de logica simbolica.
+++ Basado en un conjunto de instrucciones que definen que es vardad y que es conocido en un programa o \nproblema definido mediante la inferencia logica, pero no se le definen los pasos a seguir para la resolucion de dicho problema.
++ Lambda calculo.
+++ Pensado inicialmente como un sistema para el manejo de funciones \ny recursividad, pero se obtuvo un modelo potente y equivalente de Von Neumann.
++++ Posteriormente dio lugar a la logica combinatoria, y sento mas bases de la programacion funcional. 
++ ¿Que es un programa? 
+++ En el paradigma inperactivo es una serie de datos, una coleccion instrucciones, \nque operan sobre dichos datos \nlas distribuciones se van ejecutando en un orden adecuado \nese orden se podrán modificar según el valor de las variables es decir \nsegún el estado del cómputo que lleva todas \nlas variables y las instrucciones en ese orden 
++ ¿Que es asignacion?
+++ Decir perfectamente que x = 2 asignó \na partir de este momento \nya hasta que yo diga lo contrario x vale 2 pero yo puedo incluso hacer \nuna asignación del tipo x es x más 2 que sean programación imperativa \nsignifica toma el valor de X y modifica el valor de X con \nel resultado principal.
++ ¿Que pasa al desaparecer los conceptos? 
+++ Al desaparecer estos indirectamente desaparecen todos los conceptos que \nestán basados en ejemplos que son esas \nserie de instrucciones que se van repitiendo un número determinado \nde veces y que están \ncontrolados por el estado del computador es deci repíteme 5 veces \nes cuando se dice que se  tiene un contador
+++ al desaparecer 
++ Al no tener el concepto de \nvariable se dice que existen pero solamente sirven para \nreferirse a los parámetros entre \nlas funciones ejemplos de la función cuadrado cuadrado.
++ Ventajas 
+++ Tener un valor determinado en el programa \nentoces los programas van a depender de los valores de entrada se conoce como transferencia referencial. 
+++ El orden no es relevante 
++  En un entorno multiprocesodor se tiene una ventaja \nporque el resultado de las otras no depende de la otra.
++ Caracteristicas 
+++ Todos gira alrededor de las funciones
+++ Las funciones puede ser \nparametros de otras funciones.  
+++ La salida de una funcion es una funcion.
+++ Todas las funciones devuelven funciones.
@endtmindmap
```



Materia: Programacioón Lógica y Funcional.

Elaborado por: Martha Valeria Ramirez Garcia. 

Numero de control: 17161209
